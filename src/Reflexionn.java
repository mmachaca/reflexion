import java.lang.reflect.*;


public class Reflexionn {

	public static void main(String[] args) {
		Class clase;
		Field campo, campos[];
		Method metodo, metodos[];
		
		try {
			 clase = Class.forName("A");
			 
			 System.out.println(clase.getClass() + " " + clase.getSimpleName() + " {\n");
			 campos = clase.getFields();
			 for (int i=0; i < campos.length; i++) {
				 campo = campos[i];
				 System.out.println("\t" + campo.getName() + " (" + campo.getType().getSimpleName() + ")");
			 }

			 System.out.println();
			
			 metodos = clase.getMethods(); 
			 
			 for (int i=0; i < metodos.length; i++) {
				 metodo = metodos[i];
				 if (metodo.getName() == "wait" ) {
					 i = metodos.length;
				 }
				 else {
					 System.out.print("\t" + metodo.getName() + " (");
					 
					 //Parametros
					 Class parametros[] = metodo.getParameterTypes();
					 for (int j=0; j < parametros.length; j++) {
						 System.out.print(parametros[j].getSimpleName());
						 if (j < parametros.length-1) System.out.print(", ");
					 }
					 
					 System.out.print(") = " + metodo.getReturnType().getSimpleName());
					 
					 //Excepciones
					 Class excepciones[] = metodo.getExceptionTypes();
					 System.out.print(" [");
					 for (int j=0; j < excepciones.length; j++) {
						 System.out.print(excepciones[j].getName());
						 if (j < excepciones.length-1) System.out.print(", ");
					 }
					 System.out.println("]");
				 }
			 }
		} 
		catch (ClassNotFoundException e) {
			 System.out.println("No se ha encontrado la clase. " + e);
		}
		System.out.print("\n}");	 
		
	}

}
